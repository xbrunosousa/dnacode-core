FROM node:12.0-slim

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

WORKDIR /usr/src/app
# COPY package.json /usr/src/app/
COPY . /usr/src/app
RUN yarn

# CMD ["yarn", "start"]
CMD yarn sequelize db:migrate; yarn sequelize db:seed:all; yarn start

EXPOSE 4000
# CMD yarn sequelize db:seed:all; yarn start