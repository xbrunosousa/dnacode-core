## Core DNACode Shop

- [Core DNACode Shop](#core-dnacode-shop)
- [Demo](#demo)
- [Instalação](#instala--o)
    + [Requisitos](#requisitos)
  * [Passos para executar localmente com Docker e Docker compose](#passos-para-executar-localmente-com-docker-e-docker-compose)
  * [Passos para executar localmente sem Docker](#passos-para-executar-localmente-sem-docker)
- [Endpoints](#endpoints)
- [Dependencias usadas:](#dependencias-usadas-)


## Demo
* É possível visualizar uma demonstração da API em produção [aqui](http://colar_url_aqui.com).

## Instalação

#### Requisitos
* Node v14+;
* Npm ou Yarn
* Docker e Docker compose (opcional)

### Passos para executar localmente com Docker e Docker compose

- Em seu terminal, na pasta do projeto, execute o comando abaixo:
```sh
$ COMPOSE_PROJECT_NAME=dnacode docker-compose up
```
- A lista de produtos pode ser visualizada no browser acessando a URL abaixo:

```sh
http://127.0.0.1:4000/products
```

### Passos para executar localmente sem Docker
- Em seu terminal, na pasta do projeto, execute o comando abaixo:
```sh
$ npm i
```

- Duplique o arquivo `.env.example` e nomeie-o como `.env`
```sh
$ cp .env.example .env
```

- Lembre-se de ajustar o conteúdo do arquivo .env, como credenciais do banco de dados postgres.

- Para executar as migrations do banco de dados, execute no terminal o comando
```sh
$ npm run sequelize db:migrate
```

- Para popular o banco de dados com produtos, execute no terminal o comando
```sh
$ npm run sequelize db:seed:all
```

- Inicie o servidor:
```sh
$ npm run start
```
- A lista de produtos pode ser visualizada no browser acessando a URL abaixo:

```sh
http://127.0.0.1:4000/products
```

## Endpoints
* O endpoint base local é `http://127.0.0.1:4000`
* Listar produtos: `GET /products/`
* Recuperar produto por código: `GET /products/:code`



## Dependencias usadas:
* [Docker](https://www.docker.com)
* [Docker compose](https://docs.docker.com/compose/)
* [Typescript](https://www.npmjs.com/package/typescript)
* [Cors](https://www.npmjs.com/package/cors)
* [dotenv](https://www.npmjs.com/package/dotenv)
* [Express](https://www.npmjs.com/package/express)