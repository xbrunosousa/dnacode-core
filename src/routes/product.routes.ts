import { Application } from 'express';
import ProductController from '../controllers/product.controller';

export class ProductRoutes {
  private productController: ProductController;

  constructor(private app: Application) {
    this.productController = new ProductController();
    this.routes();
  }

  public routes() {
    this.app.route('/products').get(this.productController.index);
    this.app.route('/products').post(this.productController.store);
    this.app.route('/products/:code').get(this.productController.get);
    this.app.route('/products/:code').delete(this.productController.destroy);
  }
}
