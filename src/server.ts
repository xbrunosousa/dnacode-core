import app from './app';
import config from './config';

const { port } = config;

app.listen(process.env.PORT || port, () =>
  console.log(`⚡️[server]: Running at port ${process.env.PORT || port}`)
);
