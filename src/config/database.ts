import { Options } from 'sequelize';

interface Env {
  DB_HOST?: string;
  DB_USER?: string;
  DB_PASSWORD?: string;
  DB_NAME?: string;
  DB_PORT?: number;
}

const {
  DB_HOST: host,
  DB_USER: username,
  DB_PASSWORD: password,
  DB_NAME: database,
  DB_PORT: port
}: Env = process.env;

const config: Options = {
  dialect: 'postgres',
  dialectOptions: { decimalNumbers: true },
  host,
  username,
  password,
  database,
  port,
  define: { timestamps: true }
};

export default config;
