'use strict';

const shortid = require('shortid');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     */
    await queryInterface.bulkInsert('products', [
      {
        code: shortid.generate(),
        name: 'iPhone 12',
        image: 'https://via.placeholder.com/400x180',
        value: 12000.00,
        createdAt: new Date(),
      },
      {
        code: shortid.generate(),
        name: 'Macbook Air',
        image: 'https://via.placeholder.com/400x180',
        value: 18000.00,
        createdAt: new Date(),
      },
      {
        code: shortid.generate(),
        name: 'AirPods Pro',
        image: 'https://via.placeholder.com/400x180',
        value: 2100.00,
        createdAt: new Date(),
      },
      {
        code: shortid.generate(),
        name: 'iPad PRO',
        image: 'https://via.placeholder.com/400x180',
        value: 11500.00,
        createdAt: new Date(),
      },
      {
        code: shortid.generate(),
        name: 'SmartCase iPad',
        image: 'https://via.placeholder.com/400x180',
        value: 225.00,
        createdAt: new Date(),
      },
      {
        code: shortid.generate(),
        name: 'Cabo Lightning USB-C',
        image: 'https://via.placeholder.com/400x180',
        value: 200.00,
        createdAt: new Date(),
      },
      {
        code: shortid.generate(),
        name: 'Apple TV 4K',
        image: 'https://via.placeholder.com/400x180',
        value: 2000.00,
        createdAt: new Date(),
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('products', null, {});
  }
};
