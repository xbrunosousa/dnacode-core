import { Response } from 'express';

export default class MainController {
  public successData(
    res: Response,
    data: Object | Array<Object>,
    pagination: Object | null = null
  ) {
    interface SuccessDataKeys {
      success?: boolean;
      data: Object | Array<Object>;
      pagination?: Object | null;
    }

    let response: SuccessDataKeys = {
      success: true,
      data: data
    };

    console.log(response);

    if (pagination) response.pagination = pagination;
    res.json(response);
  }

  public error(res: Response, msg: string, statusCode: number = 500) {
    interface ErrorKeys {
      success: boolean;
      msg: string;
    }

    const response: ErrorKeys = { success: false, msg };

    res.json(response).status(statusCode);
  }
}
