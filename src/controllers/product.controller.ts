import { Request, Response } from 'express';
import Product from '../models/product.model';
import MainController from './main.controller';

export default class ProductController extends MainController {
  public async index(req: Request, res: Response) {
    Product.findAll()
      .then(products => {
        res.json({ success: true, data: products });
      })
      .catch(err => res.json(err).status(500));
  }

  public store(req: Request, res: Response) {
    res.json({ ok: 'ok' });
  }

  public get = async (req: Request, res: Response) => {
    try {
      const product = await Product.findOne(<any>{
        where: { code: req.params.code }
      });
      const { value } = product;

      res.json({
        success: true,
        data: {
          product,
          parcels: this.getParcel(value, 12, 0.45)
        }
      });
    } catch (error) {
      res.json({ success: false, msg: error.message });
    }
  };

  private getParcel(
    value: number,
    maxParcels: number,
    interestPerMonth: number
  ) {
    const factor: number = interestPerMonth * 0.01;
    let resultsWhile: Array<Object> = [];
    let parcel: number = 1;

    while (parcel <= maxParcels) {
      let math = Math.pow(1 + factor, parcel);
      let parcelValue: any =
        parcel == 1 ? value : (value * math * factor) / (math - 1);

      resultsWhile.push({
        quantity: parcel,
        amount: +parseFloat(parcelValue).toFixed(2),
        total: +(parcel * parcelValue).toFixed(2)
      });
      parcel++;
    }

    return resultsWhile;
  }

  public destroy(req: Request, res: Response) {
    res.json({ ok: 'ok' });
  }
}
