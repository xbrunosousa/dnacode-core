import express from 'express';
import { Application } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { ProductRoutes } from './routes/product.routes';
import * as dotenv from 'dotenv';

class App {
  public app: Application;
  public productRoutes: ProductRoutes;

  constructor() {
    this.app = express();
    this.setConfig();
    dotenv.config();

    // controllers
    this.productRoutes = new ProductRoutes(this.app);
  }

  private setConfig() {
    this.app.use(bodyParser.json({ limit: '50mb' }));
    this.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    this.app.use(cors());
  }
}

export default new App().app;
