import Sequelize, { Model } from 'sequelize';
import shortid from 'shortid';
import database from '../db';

class Product extends Model {
  public id!: number;
  public code!: string;
  public name!: string;
  public image!: string;
  public value!: number;

  public createdAt!: Date;
  public updatedAt!: Date;
}

Product.init(
  {
    name: Sequelize.STRING,
    code: Sequelize.STRING,
    image: Sequelize.STRING,
    value: Sequelize.FLOAT
  },
  {
    sequelize: database.connection,
    freezeTableName: true,
    tableName: 'products'
  }
);

Product.addHook(
  'beforeCreate',
  async (product: Product): Promise<void> => {
    product.createdAt = new Date();
  }
);
Product.addHook(
  'beforeCreate',
  async (product: Product): Promise<void> => {
    product.code = shortid.generate();
  }
);
Product.addHook(
  'beforeUpdate',
  async (product: Product): Promise<void> => {
    product.updatedAt = new Date();
  }
);

export default Product;
